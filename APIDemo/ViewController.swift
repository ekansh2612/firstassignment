//
//  ViewController.swift
//  APIDemo
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON



    
    /*struct CellData {
     let gameDate : String
     let gameTime : String
     let teamA : String
     let teamB : String
     let gameLocation : String
     let coordinates : String
     } */
    
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
        
    var gameData = [AnyObject]()
        @IBOutlet weak var tableView: UITableView!
        
    
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
            Alamofire.request("https:fifa-api-7e390.firebaseio.com/Matches.json").responseJSON {response in
                
                
                guard let apiData = response.result.value as? Dictionary<String,AnyObject>else {
                    print("Error getting data from the URL")
                    return
                }
                if let innerDict = apiData["matches"] {
                    self.gameData = innerDict as! [AnyObject]
                    self.tableView.reloadData()
                   /* let jsonResponse = JSON(apiData)
                  //  let sunriseTime = jsonResponse["results"]["sunrise"].string
                 //   let sunsetTime = jsonResponse["results"]["sunset"].string
                    self.date = jsonResponse["Matches"]["Final"]["DateofGame"].string ?? "abc"
                */
            }
            
        }
            
    }// didload class
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return gameData.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomTableViewCell
        
            
      /*       if gameData["Matches"]?["Final"] != nil {
             let final = gameData["Matches"]?["Final"] as? Dictionary<String,AnyObject>
             if final["DateofGame"] != nil{
             let date = final["DateofGame"] as String
             }*/
            
            let date = gameData[indexPath.row]["DateOfGame"]
            let city = gameData[indexPath.row]["Location"]
            let time = gameData[indexPath.row]["TimeOfGame"]
            let coordinates = gameData[indexPath.row]["Longitude&Latitude"]
            let teamA = gameData[indexPath.row]["TeamA"]
            let teamB = gameData[indexPath.row]["TeamB"]
            
            
            cell?.dateLabel.text = date as? String
            cell?.cityLabel.text = city as? String
            cell?.timeLabel.text = time as? String
            cell?.coordinateLabel.text = coordinates as? String
            cell?.teamALabel.text = teamA as? String
            cell?.teamBLabel.text = teamB as? String
            
            return cell!
        }
    
    
    @IBAction func buttonPressed(_ sender: Any) {
    }
    
        
    }//end of class




